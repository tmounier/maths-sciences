<html><head><meta charset='utf-8'><meta id="ForPhone" name="viewport" content="width=device-width, initial-scale=1.0"><title>Partage Maths Sciences</title> </head>

  

  
  
<body>
  
  
# Maths / Sciences au LP

  <br>
 
  
<img src="https://forge.aeif.fr/tmounier/maths-sciences/-/raw/main/avatar2.jpg" width="200" height="200" alt="Thomas Mounier, PLP Maths / Sciences" title="Thomas Mounier, PLP Maths / Sciences">

  
<div> 
 Bienvenue ! Je suis enseignant maths/sciences en Lycée Professionnel dans l'académie de Lyon et vous propose une mise à disposition d'une bonne partie de mes ressources utilisées avec mes élèves.  
  <br>
Cette page n'est qu'une entrée vers différents dépôts sur la Forge selon mes niveaux. 
  <br>
  En septembre 2023 je reprends l'aspect de tous mes documents, c'est pourquoi vous ne trouverez l'ensemble qu'au fur et à mesure de l'année.
</div>

  <br>
  
<p><strong>Du LaTeX et du PDF</strong></p>
  

<div> Les documents sont disponibles, la plupart du temps, au format .tex (LaTeX) ou au format .pdf pour une utilisation direct en cas de besoin.
<br>
Je ne travaille plus sous traitement de texte classique depuis quelques années, mais même pour un débutant en LaTeX, vous trouverez tous les éléments pour compiler. Il vous faut : les fichiers images / le .tex de la ressource et les packages contenus dans le dossier éponyme.

  </div><div>
Un projet est de proposer un tuto pour compiler simplement des documents trouvés sur le net, mais manque de temps !

  </div>

---
  
## Les liens
  

1. [Seconde bac pro](https://tmounier.forge.aeif.fr/2nd-tex/) (maths pour 2023/2024)
2. [Première bac pro](https://tmounier.forge.aeif.fr/1ere-tex/) (maths/sciences Groupement B)
3. [Terminale bac pro](https://tmounier.forge.aeif.fr/term-tex/) (maths/sciences Groupement B)
4. [BTS](https://tmounier.forge.aeif.fr/bts-tex/) (bâtiment, physique, en construction)  
5. [Secondes, nouvel espace 24/25](https://maths-sciences-en-lp.forge.apps.education.fr/seconde-bac-pro/) (refonte du premier lien)


## Des ressources libres
  
  
En attente d'une plus profonde réflexion (et gestion des conflits), mes ressources sont distribuées sous licence 
  ![](https://forge.aeif.fr/tmounier/maths-sciences/-/raw/main/ccby.png)


  </body></html>
